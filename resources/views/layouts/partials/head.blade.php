<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tokko</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="/css/main.css">
    <script 
      type="text/javascript"
      src="https://app.sandbox.midtrans.com/snap/snap.js"
      data-client-key="{{ config('tokko.services.midtrans.client_key') }}"
    ></script>
    @livewireStyles
</head>
