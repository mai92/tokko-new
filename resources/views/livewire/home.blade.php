 <div class="flex flex-wrap">
  <div class="w-full md:w-2/12 sr-only md:not-sr-only">
    <div class="bg-white mt-2 px-8 py-6">
      <h1 class="font-bold border-b text-xl">
        Kategori
      </h1>
      @foreach ($categories as $categoryProduct)
          <a
          wire:click="selectCategory({{ $categoryProduct->id }})"
          class="my-2 flex hover:text-pink-600
          @if($category == $categoryProduct->id)
            text-pink-600
          @else
            text-gray-700
          @endif"
          >
            {{ $categoryProduct->name }} ({{ $categoryProduct->products->count() }})
          </a>
      @endforeach
    </div>
  </div>

  <div class="w-full md:w-10/12">
    <div class="flex flex-wrap mt-2">
      <input type="text" wire:model="search" class="focus:outline-none focus:shadow-outline border py-2 px-4 block w-full mb-2 mx-2" placeholder="Cari produk...">
      @foreach ($products as $product)
          <div class="w-1/2 md:w-1/3 lg:w-1/4 p-2">

              <div class="flex justify-center flex-col bg-white shadow-md p-4 hover:bg-gray-200">
                <div class="flex justify-center">
                  <a href="{{ route('product.detail', $product) }}">
                    <img src="{{ $product->getImage() }}" alt="{{ $product->name }}" class="h-32 w-32 md:h-48 md:w-48">
                  </a>
                </div>

                <div class="text-left">
                  <span class="font-bold">{{ $product->name }}</span>
                  <p>{{ $product->description }}</p>
                  <p class="text-red-400">{{ format_rupiah($product->price) }}</p>
                </div>
                <button
                wire:click="addToCart({{ $product->id }})"
                class="p-2 bg-pink-600 hover:bg-pink-500 text-white">
                Add to Cart
                </button>
              </div>
          </div>
      @endforeach
    </div>
    {{ $products->links('vendor.pagination.tailwind') }}
  </div>

</div>
