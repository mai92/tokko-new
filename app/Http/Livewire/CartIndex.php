<?php

namespace App\Http\Livewire;

use App\Product;
use App\Support\Facades\Cart;
use Livewire\Component;

class CartIndex extends Component
{
    public $cart;
    public $grandTotal = 0;

    public function mount()
    {
        $this->cart = Cart::get();
        $this->grandTotal = collect($this->cart['products'])->sum('price');
    }

    public function render()
    {
        return view('livewire.cart-index', [
            'items' => Cart::groupedItems()->sortBy('name'),
        ]);
    }

    public function clearCart()
    {
        Cart::clear();
        $this->updateCart();
    }

    public function removeItem(int $productId)
    {
        Cart::removeItem($productId);
        $this->updateCart();
    }

    public function subtractQty(int $productId)
    {
        Cart::remove($productId);
        $this->updateCart();
    }

    public function addQty(int $productId)
    {
        Cart::add(Product::find($productId));
        $this->updateCart();
    }

    private function updateCart()
    {
        $this->cart = Cart::get();
        $this->grandTotal = collect($this->cart['products'])->sum('price');
        $this->emit('updateCartTotal');
    }
}
