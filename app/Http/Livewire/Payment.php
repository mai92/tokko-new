<?php

namespace App\Http\Livewire;

use App\Order;
use Illuminate\Support\Facades\Http;
use Livewire\Component;

class Payment extends Component
{
    public $order;

    public function mount(Order $order)
    {
        $this->order = $order;
    }

    public function render()
    {
        return view('livewire.payment');
    }

    public function pay($orderId)
    {
        $order = Order::find($orderId);

        // if ($order->snap_token !== null) {
        //     $this->dispatchBrowserEvent('payment', ['token' => $order->snap_token]);
        //     return;
        // }

        $payment = Http::withBasicAuth(
            config('tokko.services.midtrans.server_key'),
            ''
            )
            ->post(config('tokko.services.midtrans.endpoint') . 'transactions', [
            'transaction_details' => [
                'gross_amount' => $order->total,
                'order_id' => $order->id,
            ],
            'item_details' => $order->details->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->product->name,
                    'price' => $item->price,
                    'quantity' => $item->qty,
                ];
            }),
            'customer_details' => [
                'first_name' => $order->user_name,
                'email' => $order->user_email,
                'phone' => $order->user_phone,
                'shipping_address' => [
                    'first_name' => $order->user_name,
                    'email' => $order->user_email,
                    'phone' => $order->user_phone,
                    'address' => $order->user_address,
                ],
            ]
        ])->json();

        $order->update(['snap_token' => $payment['token']]);
        $this->dispatchBrowserEvent('payment', ['token' => $payment['token']]);
        return;
    }
}
