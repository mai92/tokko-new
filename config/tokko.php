<?php 

return [
    'services' => [
        'rajaongkir' => [
            'api_key' => env('RAJAONGKIR_APIKEY', null),
            'origin' => env('RAJAONGKIR_ORIGIN', null),
        ],
        'midtrans' => [
            'endpoint' => env('MIDTRANS_ENDPOINT', 'https://app.sandbox.midtrans.com/snap/v1/'),
            'client_key' => env('MIDTRANS_CLIENT_KEY', null),
            'server_key' => env('MIDTRANS_SERVER_KEY', null),
            'merchant_id' => env('MIDTRANS_MERCHANT_ID', null),
        ]
    ],
];